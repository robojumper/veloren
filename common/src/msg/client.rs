use vek::*;
use super::ClientState;
use crate::comp;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ClientMsg {
    Register { player: comp::Player },
    Character(comp::Character),
    RequestState(ClientState),
    Ping,
    Pong,
    Chat(String),
    PlayerAnimation(comp::character::AnimationHistory),
    PlayerPhysics {
        pos: comp::phys::Pos,
        vel: comp::phys::Vel,
        dir: comp::phys::Dir,
    },
    TerrainChunkRequest {
        key: Vec3<i32>,
    },
    Disconnect,
}
